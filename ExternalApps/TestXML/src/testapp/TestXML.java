/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testapp;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import testapp.xml.XmlParser;

/**
 *
 * @author gmimano
 */
public class TestXML {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String xml = "<DocumentElement>"+
                        "<VaccineLots>"+
                    "<CODE>037G9057</CODE>"+
    "<CODE>02129001A</CODE>"+
    "</VaccineLots>"+
    "</DocumentElement>";
        
        InputStreamReader reader = new InputStreamReader(new ByteArrayInputStream(xml.getBytes()));
        
        XmlParser parser = new XmlParser();
        
        
        
        Object res = parser.processXml(reader);
        
        System.out.println("Running=>"+res.toString());
        
        String[] array = res.toString().split(",");
        
        System.out.println("Running WITH=>"+array.length);
        
        
        
    }
}
