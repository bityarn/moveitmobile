/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testapp.model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Vector;

/**
 *
 * @author gmimano
 */
public class LotNumbers {
    private Vector lotNumbers;

    public LotNumbers() {
        lotNumbers = new Vector(0);
    }
    
    public void addLot(String lotNum){
        lotNumbers.addElement(lotNum);
        
    }
    
    public String[] getLotNumbers(){
        String[] nums = new String[lotNumbers.size()];
        
        lotNumbers.copyInto(nums);
        
        
        return nums;
    }
    
    public Vector getLotVector(){
        return lotNumbers;
    }

    public void write(DataOutputStream stream) throws IOException {
        
    }

    public void read(DataInputStream stream) throws IOException, InstantiationException, IllegalAccessException {
        if (!(lotNumbers!=null)) {
            lotNumbers=new Vector(0);
        }
        String num;
        boolean isRead=true;
        
        while(isRead){
            try {
                num = stream.readUTF();
                lotNumbers.addElement(num);
            } catch (Exception e) {
                isRead=false;
            }
        }
    }
    
    
    
     
    
}
