package testapp.xml;

import org.kxml2.kdom.Element;

public class MetaDataHandler implements IElementHandler {
	private MetaData myMetaData = new MetaData();
	
	

	
	//in this funtion u retrieve from the metadata tag and construct ur metadata object
	@Override
	public Object handle(Element e) {
		
		
		for (int i = 0; i < e.getChildCount(); i++) {
			if (e.getType(i) == Element.ELEMENT) {
				if(e.getName().equals("id")){
					myMetaData.setId("id"); //retireve id and store in object
					
				}else if (e.getName().equals("name")) {
					myMetaData.setName("A name"); //retrieve name and store
				}
			}
		}
		return myMetaData;

	}

}
